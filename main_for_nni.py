import nni

import pandas as pd

df = pd.read_csv("drinking_water_potability.csv")
df_missing_dropped = df.dropna()

from sklearn.model_selection import train_test_split
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import SimpleImputer, IterativeImputer, KNNImputer
from sklearn.preprocessing import StandardScaler, Normalizer, MinMaxScaler
from imblearn.over_sampling import BorderlineSMOTE,SVMSMOTE, ADASYN, KMeansSMOTE

from sklearn.metrics import fbeta_score, accuracy_score

from sklearn.tree import DecisionTreeClassifier
from sklearn.ensemble import ExtraTreesClassifier, RandomForestClassifier, BaggingClassifier, AdaBoostClassifier, GradientBoostingClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

RECEIVED_PARAMS = nni.get_next_parameter()


possible_missing_strategy = {'drop':'drop',
                            'mean':SimpleImputer(strategy='mean'),
                            'median':SimpleImputer(strategy='median'),
                            'most_frequent':SimpleImputer(strategy='most_frequent'),
                            'iterative_sample':IterativeImputer(sample_posterior=True),
                            'iterative_no_sample':IterativeImputer(sample_posterior=False),
                            'knn2':KNNImputer(n_neighbors=2),'knn5':KNNImputer(n_neighbors=5),
                            'knn10':KNNImputer(n_neighbors=10)}

possible_augmentation_strategy = {'none':None,
                                'borderline':BorderlineSMOTE(),
                                'svm':SVMSMOTE(),
                                'adasyn':ADASYN(),
                                'kmeans':KMeansSMOTE(cluster_balance_threshold=0.45)}

possible_scale_strategy = {'none':None,
                            'l1norm':Normalizer(norm='l1'),
                            'l2norm':Normalizer(norm='l2'),
                            'max':Normalizer(norm='max'),
                            'std':StandardScaler(),
                            'minmax':MinMaxScaler()}

possible_models = {}

# Add svm if necessary
try:
    kernel = RECEIVED_PARAMS['kernel']
    C = RECEIVED_PARAMS['C']
    degree = RECEIVED_PARAMS['degree']
    possible_models['svm'] = SVC(kernel=kernel, C=C, degree=degree)
except:pass

# Add decision_tree, extra_tree, random forest, bagging if necessary
try:
    max_depth = RECEIVED_PARAMS['max_depth']
    min_samples_split = RECEIVED_PARAMS['min_samples_split']
    possible_models['decision_tree'] = DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split)
    possible_models['extra_tree'] = ExtraTreesClassifier(max_depth=max_depth, min_samples_split=min_samples_split)
    possible_models['random_forest'] = RandomForestClassifier(n_estimators=100, max_depth=max_depth, min_samples_split=min_samples_split)
    possible_models['bagging'] = BaggingClassifier(base_estimator=DecisionTreeClassifier(max_depth=max_depth, min_samples_split=min_samples_split), n_estimators=100)
except:pass

# Add knn if necessary
try:
    n_neighbors = RECEIVED_PARAMS['n_neighbors']
    possible_models['knn'] = KNeighborsClassifier(n_neighbors=n_neighbors)
except:pass

# Add gaussian_nb if necessary
try:
    var_smoothing = RECEIVED_PARAMS['var_smoothing']
    possible_models['gaussian_nb'] = GaussianNB(var_smoothing=var_smoothing)
except:pass

# Add adaboost, gradientboost if necessary
try:
    learning_rate = RECEIVED_PARAMS['learning_rate']
    possible_models['adaboost'] = AdaBoostClassifier(n_estimators=100, learning_rate=learning_rate)
    possible_models['gradientboost'] = GradientBoostingClassifier(n_estimators=100, learning_rate=learning_rate)
except:pass

missing_strategy = possible_missing_strategy[RECEIVED_PARAMS['missing_strategy']]
augmentation_strategy = possible_augmentation_strategy[RECEIVED_PARAMS['augmentation_strategy']]
scale_strategy = possible_scale_strategy[RECEIVED_PARAMS['scale_strategy']]
model = possible_models[RECEIVED_PARAMS['model']]

def preprocess_data(missing_strategy, scale_strategy, augmentation_strategy):

    # Missing strategy
    if missing_strategy == 'drop':
        train_set, test_set = train_test_split(df_missing_dropped, test_size=0.15, random_state=42)
        X_train = train_set.drop("Potability", axis=1)
        y_train = train_set["Potability"].copy()
        X_test = test_set.drop("Potability", axis=1)
        y_test = test_set["Potability"].copy()
    else:
        train_set, test_set = train_test_split(df, test_size=0.15, random_state=42)
        X_train = train_set.drop("Potability", axis=1)
        y_train = train_set["Potability"].copy()
        X_test = test_set.drop("Potability", axis=1)
        y_test = test_set["Potability"].copy()

        imputer = missing_strategy
        imputer.fit(X_train)
        X_train = imputer.transform(X_train)
        X_test = imputer.transform(X_test)
    
    # Data augmentation strategy
    if augmentation_strategy != None:
        X_train, y_train = augmentation_strategy.fit_resample(X_train,y_train)

    # Scale strategy
    if scale_strategy != None:
        scaler = scale_strategy
        scaler.fit(X_train)
        X_train = scaler.transform(X_train)
        X_test = scaler.transform(X_test)

    return X_train, y_train, X_test, y_test

def compute_metrics(model,missing_strategy,scale_strategy,augmentation_strategy):
    X_train, y_train, X_test, y_test = preprocess_data(missing_strategy,scale_strategy,augmentation_strategy)
    model.fit(X_train, y_train)
    y_pred_train = model.predict(X_train)
    y_pred_test = model.predict(X_test)
    accuracy_train = accuracy_score(y_train, y_pred_train)
    f1score_train = fbeta_score(y_train, y_pred_train, beta=0.5)
    accuracy_test = accuracy_score(y_test, y_pred_test)
    f1score_test = fbeta_score(y_test, y_pred_test, beta=0.5)
    metrics = {'default':f1score_test,'accuracy_train':accuracy_train,'fscore_train':f1score_train,'accuracy_test':accuracy_test,'fscore_test':f1score_test}
    return metrics

metrics = compute_metrics(model,missing_strategy,scale_strategy,augmentation_strategy)

nni.report_final_result(metrics)